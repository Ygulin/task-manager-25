package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.service.IAuthService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.IUserService;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.exception.field.LoginEmptyException;
import ru.tsc.gulin.tm.exception.field.PasswordEmptyException;
import ru.tsc.gulin.tm.exception.field.PasswordIncorrectException;
import ru.tsc.gulin.tm.exception.system.AccessDeniedException;
import ru.tsc.gulin.tm.exception.system.AuthenticationException;
import ru.tsc.gulin.tm.exception.system.PermissionException;
import ru.tsc.gulin.tm.model.User;
import ru.tsc.gulin.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @NotNull final User user = Optional.ofNullable(userService.findOneByLogin(login))
                .orElseThrow(AuthenticationException::new);
        if (user.getLocked()) throw new PermissionException();
        if (!user.getPasswordHash().equals(HashUtil.salt(propertyService, password))) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
